#pragma once
#include <iostream>
#include "wrinkles.h"  

int main() {
	Wrinkles wrinkles;
	std::vector<std::vector<double>> filter = wrinkles.createGaussianFilter(3, 3, 1);

	//get original and four binary images
	cv::Mat image = cv::imread("sobel2.png", CV_8UC1);
	cv::Mat foreheadWhite = cv::imread("forehead6.png", CV_8UC1);
	cv::Mat crowsFeetWhite = cv::imread("crows6.png", CV_8UC1);
	cv::Mat underEyesWhite = cv::imread("eyes6.png", CV_8UC1);
	cv::Mat aroundMouthWhite = cv::imread("cheeks6.png", CV_8UC1);

	std::vector<cv::Mat> fourBinaryImages = { foreheadWhite, crowsFeetWhite, underEyesWhite, aroundMouthWhite };

	//new image for histogram equalisation
	cv::Mat histImage(image.size(), CV_8UC1);
	wrinkles.histogramEqualisation(image, histImage);

	cv::Mat gaussianBlurImage(histImage.rows, histImage.cols, histImage.type());

	//apply gaussian blur on the entire image
	wrinkles.applyGaussFilter(histImage, gaussianBlurImage, filter);

	//make new four images
	cv::Mat forehead = cv::Mat(gaussianBlurImage.size(), CV_8UC1);
	cv::Mat crowsFeet = cv::Mat(gaussianBlurImage.size(), CV_8UC1);
	cv::Mat underEyes = cv::Mat(gaussianBlurImage.size(), CV_8UC1);
	cv::Mat aroundMouth = cv::Mat(gaussianBlurImage.size(), CV_8UC1);

	std::vector<cv::Mat> fourNewImages = { forehead, crowsFeet, underEyes, aroundMouth };

	for (int i = 0; i < fourNewImages.size(); i++) {
		wrinkles.sobelOperator(gaussianBlurImage, fourBinaryImages[i], fourNewImages[i]);
		wrinkles.countPossibleWrinkles(fourNewImages[i], fourBinaryImages[i], 40);
	}


	imshow("banana", forehead);
	return 0;
}