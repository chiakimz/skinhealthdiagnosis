#pragma once
#include "wrinkles.h"

Wrinkles::Wrinkles()
{
}
const double PI = 3.141;

void Wrinkles::histogramEqualisation(cv::Mat &image, cv::Mat &newImage) {
	float histogram[256]; //array of 0-255
	//I don't understand the below line
	memset(histogram, 0, sizeof(float) * 256); //z ero the array
	for (int i = 0; i < image.rows * image.cols; i++) { //go through the entire image
		int index = image.data[i]; //get the uchar of that pixel
		histogram[index]++; //add 1 to the uchar of that pixel in this histogram array
	}
	for (int i = 1; i < 256; i++) {
		histogram[i] += histogram[i - 1]; //add the number of uchars next to each other
	}
	for (int i = 0; i < 256; i++) {
		histogram[i] /= histogram[255]; //devide with the number of white pixels?
	}
	for (int i = 0; i < image.rows * image.cols; i++) {
		newImage.data[i] = histogram[image.data[i]] * 255; //store new pixels to the new image
	}
}
std::vector<std::vector<double>> Wrinkles::createGaussianFilter(int row, int column, double sigma) {
	std::vector<std::vector<double>> gaussFilter;

	for (int i = 0; i < row; i++) {
		std::vector<double>col;
		for (int j = 0; j < column; j++) {
			col.push_back(-1);
		}
		gaussFilter.push_back(col);
	}

	float coordSum = 0;
	double constant = 2.0 * sigma * sigma;

	float sum = 0.0;

	for (int x = -row / 2; x <= row / 2; x++) {
		for (int y = -column / 2; y <= column / 2; y++) {
			coordSum = (x * x + y * y);
			gaussFilter[x + row / 2][y + column / 2] = (exp(-(coordSum) / constant)) / (PI * constant);
			sum += gaussFilter[x + row / 2][y + column / 2];

		}
	}

	for (int i = 0; i < row; i++)
		for (int j = 0; j < column; j++)
			gaussFilter[i][j] /= sum;

	return gaussFilter;
}

cv::Mat Wrinkles::applyGaussFilter(cv::Mat &inputImage, cv::Mat &outputImage, std::vector<std::vector<double>> filter) {
	//cvtColor(inputImage, inputImage, CV_RGB2GRAY);
	int size = ((int)filter.size() - 1) / 2;
	outputImage = cv::Mat(inputImage.rows - 2 * size, inputImage.cols - 2 * size, CV_8UC1);
	for (int i = size; i < inputImage.rows - size; i++) {
		for (int j = size; j < inputImage.cols - size; j++) {

			float sum = 0;

			for (int x = 0; x < filter.size(); x++)
				for (int y = 0; y < filter.size(); y++) {
					sum += filter[x][y] * (float)(inputImage.at<uchar>(i + x - size, j + y - size));
				}
			outputImage.at<uchar>(i - size, j - size) = sum;
		}
	}
	return outputImage;
}

void Wrinkles::sobelOperator(cv::Mat &inputImage, cv::Mat &binaryImage, cv::Mat &outputImage) {
	//set all pixels white
	outputImage = (outputImage.size(), CV_8UC1, cv::Scalar(255));

	//x direction kernel
	std::vector<std::vector<int>> xKernel = { {-1, 0, 1},
											  {-2, 0, 2},
											  {-3, 0, 3},
											  {-2, 0, 2},
											  {-1, 0, 1} };

	//y direction kernel
	std::vector<std::vector<int>> yKernel = { {-1, -2, -3, -2, -1},
											  {0, 0, 0, 0, 0},
											  {1, 2, 3, 2, 1} };


	for (int y = 2; y < inputImage.cols - 2; y++) {
		for (int x = 2; x < inputImage.rows - 2; x++) {
			//skip unnecessary pixels
			if (binaryImage.at<uchar>(x, y) < 128)
				continue;

			float totalX = 0;
			float totalY = 0;

			int currentY = y - 2;
			for (int i = 0; i < xKernel.size(); i++) {
				int currentX = x - 1;
	
				for (int j = 0; j < xKernel[i].size(); j++) {
					totalX += inputImage.at<uchar>(currentX, currentY) * xKernel[i][j];
					currentX++;
				}
				currentY++;
			}
			
			if (x > 2 && x < inputImage.cols - 2) {
			
				currentY = y - 1;
				for (int i = 0; i < yKernel.size(); i++) {
					int currentX = x - 2;

					for (int j = 0; j < yKernel[i].size(); j++) {
						totalY += inputImage.at<uchar>(currentX, currentY) * yKernel[i][j];
						currentX++;
					}
					currentY++;
				}
			}

			float t = sqrt(totalX * totalX + totalY * totalY);
			outputImage.at<uchar>(x, y) = t > 255 ? 255 : t;
		}
	}
}

void Wrinkles::countPossibleWrinkles(cv::Mat &inputImage, cv::Mat &binaryImage, int threshold) {

	possibleWrinklesPixels = 0;
	justPixels = 0;

	for (int i = 1; i < inputImage.rows - 1; i++) {
		for (int j = 1; j < inputImage.cols - 1; j++) {

			//skip unnecessary pixels
			if (inputImage.at<uchar>(i, j) == 255 )
				continue;

			if (inputImage.at<uchar>(i, j) > threshold) {
				possibleWrinklesPixels++;
			}
			else {
				justPixels++;
			}
		}
	}
	howWrinklyPercentage = (possibleWrinklesPixels / (possibleWrinklesPixels + justPixels)) * 100;

	std::cout << "Wrinkles for this area is " << howWrinklyPercentage << " % " << std::endl;
}
Wrinkles::~Wrinkles()
{
}


////x direction gradient
//float totalX = ((float)inputImage.at<uchar>(i, j + 1) * 2 + (float)inputImage.at<uchar>(i - 1, j + 1) + (float)inputImage.at<uchar>(i + 1, j + 1)) - ((float)inputImage.at<uchar>(i, j - 1) * 2 + (float)inputImage.at<uchar>(i - 1, j - 1) + (float)inputImage.at<uchar>(i + 1, j - 1));
//
////y direction gradient
//float totalY = ((float)inputImage.at<uchar>(i + 1, j) * 2 + (float)inputImage.at<uchar>(i + 1, j - 1) + (float)inputImage.at<uchar>(i + 1, j + 1)) - ((float)inputImage.at<uchar>(i - 1, j) * 2 + (float)inputImage.at<uchar>(i - 1, j - 1) + (float)inputImage.at<uchar>(i - 1, j + 1));
//

