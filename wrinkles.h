#pragma once
#include <opencv2/opencv.hpp>

class Wrinkles
{
private:
	float possibleWrinklesPixels;
	float justPixels;
	float howWrinklyPercentage;

public:
	Wrinkles();
	~Wrinkles();
	void histogramEqualisation(cv::Mat &image, cv::Mat &newImage);
	std::vector<std::vector<double>> createGaussianFilter(int row, int column, double sigma);
	cv::Mat applyGaussFilter(cv::Mat &inputImage, cv::Mat &outputImage, std::vector<std::vector<double>> filter);

	void sobelOperator(cv::Mat &inputImage, cv::Mat &binaryImage, cv::Mat &outputImage);
	//cv::Mat imagesVec(cv::Mat &image1, cv::Mat &image2, cv::Mat &image3, cv::Mat &image4);
	void countPossibleWrinkles(cv::Mat &inputImage, cv::Mat &binaryImage, int threshold);
	std::vector<cv::Mat> fourImages;


};

